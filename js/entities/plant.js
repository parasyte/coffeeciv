game.Plant = me.AnimationSheet.extend({
    "init" : function init(x, y, settings) {
        this.parent(x, y, me.loader.getImage("plant"), 64, 64);
        this.name = this.pos.x + "_" + this.pos.y;

        this.addAnimation("sprout", [ 0 ]);
        this.addAnimation("default", [ 1 ]);
        this.setCurrentAnimation("default");

        // Register click events
        me.input.registerMouseEvent("mousedown", this, this.click.bind(this));

        game.subscribe("plant", this.plant.bind(this));
    },

    "plant" : function plant(GUID, content) {
        console.log(GUID, content);
        var obj = me.game.getEntityByName(content.name)[0];
        obj.setCurrentAnimation("sprout");
    },

    "click" : function click() {
        this.setCurrentAnimation("sprout");
        game.status.current.plants[this.name] = true;
        game.publish("plant", {
            name: this.name
        })
    },

    "update" : function update() {
        return true;
    }
});
