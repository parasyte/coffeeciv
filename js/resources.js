game.resources = {
    /* Graphics. */
    "img" : [
        "tiles",
        "truck",
        "plant"
    ],

    /* Maps from Tiled. */
    "map" : [
        "farm"
    ],

    /* 2-channel audio. Usually sound effects. */
    "sfx" : [
    ],

    /* 1-channel audio. Usually music. */
    "bgm" : [
    ]
};
