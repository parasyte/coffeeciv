game.Truck = me.ObjectEntity.extend({
    "init" : function init(x, y, settings) {
        this.parent(x, y, settings);

        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;

        this.startY = y;
        this.endY = y + settings.height - settings.spriteheight;

        this.dir = "right";
        this.name = "truck";

        this.addAnimation("left", [ 0 ]);
        this.addAnimation("right", [ 1 ]);
        this.addAnimation("up", [ 2 ]);
        this.addAnimation("down", [ 3 ]);

        this.setCurrentAnimation(this.dir);
        this.setVelocity(2, 0);
    },

    "update" : function update() {
        var left = (this.dir === "left");

        if (left && this.pos.x <= this.startX) {
            this.dir = "right";
        }
        else if (!left && this.pos.x >= this.endX) {
            this.dir = "left";
        }
        this.setCurrentAnimation(this.dir);
        this.vel.x += left ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;

        this.updateMovement();

        if (this.vel.x || this.vel.y) {
            // update object animation
            this.parent(this);
            return true;
        }
        return false;
    }
});
