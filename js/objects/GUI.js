game.money = me.HUD_Item.extend({
    init: function(x, y) {
        this.parent(x, y);
        this.w = ~~(c.WIDTH / 6);
        this.h = ~~(c.HEIGHT / 10);
        this.font = new me.Font("bold Century Gothic", this.h, "white");

        this.set(game.status.current.money);
    },

    set : function set(value) {
        var result = this.parent(value);
        game.status.current.money = this.value;
        return result;
    },

    update : function update(value) {
        var result = this.parent(value);
        game.status.current.money = this.value;
        return result;
    },

    reset : function reset(value) {
        var result = this.parent(value);
        game.status.current.money = this.value;
        return result;
    },

    draw: function(context, x, y) {
        var alpha = context.globalAlpha;
        context.globalAlpha = 0.5;
        context.fillStyle = game.status.team;
        context.fillRect(this.pos.x + x, this.pos.y + y, this.w, this.h);
        context.globalAlpha = alpha;

        this.font.draw(context, "$" + this.value, this.pos.x + x, this.pos.y + y);
    }
});

game.panel = me.SpriteObject.extend({
    init: function(w, h) {
        // FIXME: should be done automagically with this.floating
        var x = c.WIDTH - w - me.game.currentLevel.pos.x;
        var y = 0 - me.game.currentLevel.pos.y;
        var canvas = document.createElement("canvas");
        canvas.width = w;
        canvas.height = h;
        this.img = canvas.getContext("2d");

        this.img.globalAlpha = 0.5;
        this.img.fillStyle = "black";
        this.img.fillRect(0, 0, w, h);
        this.parent(x, y, canvas, w, h);
        this.z = 1000;
        this.floating = true;
        me.game.add(new game.buyer_buy(x + 10, y + 10, w - 20, ~~((h / 3) - 20)));
    }
});

game.buyer_buy = me.SpriteObject.extend({
    init: function(x, y, w, h) {
        var canvas = document.createElement("canvas");
        canvas.width = w;
        canvas.height = h;
        this.img = canvas.getContext("2d");

        this.font = new me.Font("bold Century Gothic", Math.min(h / 2, w / 4), "white");

        var alpha = this.img.globalAlpha;
        this.img.globalAlpha = 0.5;
        this.img.fillStyle = game.status.team;
        this.img.fillRect(0, 0, w, h);
        this.img.globalAlpha = alpha;

        this.font.draw(this.img, "BUY", 3, 3);

        this.parent(x, y, canvas, w, h);
        this.z = 1001;
        this.floating = true;

        // Register click events
        me.input.registerMouseEvent("mousedown", this, this.click.bind(this), true);
    },

    "click" : function click() {
        // Debit some money
        me.game.HUD.updateItemValue("money", -100);

        game.publish("teamstatus", game.status.current);
    }
});
game.buyer_upgrade = me.SpriteObject.extend({});
game.buyer_land = me.SpriteObject.extend({});
