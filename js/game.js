
/* Game namespace */
var game = {
    // Run on page load.
    "onload" : function onload() {
        // Initialize the video.
        if (!me.video.init("screen", c.WIDTH, c.HEIGHT)) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
        this.loadResources();

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },

    "loadResources" : function loadResources() {
        // Set all resources to be loaded.
        var resources = [];

        // Graphics.
        this.resources["img"].forEach(function forEach(value) {
            resources.push({
                "name"  : value,
                "type"  : "image",
                "src"   : "resources/img/" + value + ".png"
            })
        });

        // Maps.
        this.resources["map"].forEach(function forEach(value) {
            resources.push({
                "name"  : value,
                "type"  : "tmx",
                "src"   : "resources/map/" + value + ".tmx"
            })
        });

        // Sound effects.
        this.resources["sfx"].forEach(function forEach(value) {
            resources.push({
                "name"      : value,
                "type"      : "audio",
                "src"       : "resources/sfx/",
                "channel"   : 2
            })
        });

        // Music.
        this.resources["bgm"].forEach(function forEach(value) {
            resources.push({
                "name"      : value,
                "type"      : "audio",
                "src"       : "resources/bgm/",
                "channel"   : 1
            })
        });

        // Load the resources.
        me.loader.preload(resources);
    },

    // Run on game resources loaded.
    "loaded" : function loaded() {
        // Set the "Select" ScreenObject.
        me.state.set(me.state.MENU, new game.SelectScreen());

        // Set the "Play" ScreenObject.
        me.state.set(me.state.PLAY, new game.PlayScreen());

        game.subscribe("teams", function (GUID, content) {
            console.log("Received team content:", content);

            if (game.status.GUIDs[GUID] === content.team) {
                return;
            }

            game.status.GUIDs[GUID] = content.team;
            game.status.teams[content.team][content.job].push(GUID);
        });

        game.subscribe("query", function (GUID, content) {
            if (game.status.team && game.status.job) {
                game.publish("teams", {
                    "team"  : game.status.team,
                    "job"   : game.status.job
                });
            }
        });
        game.publish("query");

        // Entities.
        me.entityPool.add("truck", game.Truck);
        me.entityPool.add("plant", game.Plant);

        // Start the game.
        me.state.change(me.state.MENU);
    },

    // Receive message from PubNub
    "receive" : function receive(msg) {
        if (msg.GUID === c.GUID) {
            return;
        }

        console.log("Received message:", msg);
        me.event.publish(msg.channel, [ msg.GUID, msg.content ]);
    },

    // Helper function to determine if a variable is an Object.
    "isObject" : function isObject(object) {
        try {
            return (!Array.isArray(object) && Object.keys(object));
        }
        catch (e) {
            return false;
        }
    },

    // Helper function for local subscription
    "subscribe" : function subscribe(channel, callback) {
        return me.event.subscribe(channel, callback);
    },

    "unsubscribe" : function unsubscribe(channel, callback) {
        return me.event.unsubscribe(channel, callback);
    },

    // Helper function to publish messages globally
    "publish" : function publish(channel, msg) {
        PUBNUB.publish({
            "channel" : "coffee",
            "message" : {
                channel : channel,
                content : msg || {},
                GUID    : c.GUID
            }
        });
    },

    // Game status
    "status" : {
        // Current status, Eg. money, trucks, ...
        "current" : {
            "money" : 2000,
            "trucks" : 1,
            "plants" : {}
        },

        // Team name, E.g. "red", "blue", ...
        "team"  : "",

        // Job title, E.g. "buyer", "farmer", "transport", ...
        "job"   : "",

        // Map teams to GUIDs
        "teams" : {},

        // Map GUIDs to teams
        "GUIDs" : {},

        "get_job" : function get_job(team) {
            var jobs = [ "buyer", "farmer", "transport" ],
                first = -1,
                value = Infinity;

            Object.keys(game.status.teams[team]).forEach(function (job, i) {
                var len = game.status.teams[team][job].length;
                if (len < value) {
                    first = i;
                    value = len;
                }
            });

            return jobs[first];
        },

        "get_position" : function get_position() {
            var grid = [],
                randomPosition = [];

            for (var i = 0; i < 101; i++) grid.push(i)

            randomPosition.push(Math.floor(Math.random() * grid.length));
            randomPosition.push(Math.floor(Math.random() * grid.length));

            return randomPosition;
        },

        "jobs_filled" : function (team) {
            return Object.keys(game.status.teams[team]).every(function (job) {
                return game.status.teams[team][job].length;
            }) && game.status.team && game.status.job;
        }
    }
};
