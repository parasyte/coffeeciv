game.PlayScreen = me.ScreenObject.extend({
    onResetEvent: function() {
        me.video.clearSurface(me.video.getScreenFrameBuffer(), "black");

        me.game.onLevelLoaded = this.onLevelLoaded.bind(this);

        // load a level
        me.levelDirector.loadLevel("farm");

        // HUD
        me.game.addHUD(0, 0, c.WIDTH, c.HEIGHT);
        me.game.HUD.addItem("money", new game.money(0, 0));
        me.game.add(new game.panel(~~(c.WIDTH * 0.2), c.HEIGHT));
        me.game.sort();
    },

    updateTeam : function updateTeam(GUID, content) {
        me.game.HUD.setItemValue("money", content.money);
        Object.keys(content.plants).forEach(function (plant) {
            me.game.getEntityByName(plant)[0].plant(GUID, {
                "name" : plant
            });
        });
    },

    onLevelLoaded : function onLevelLoaded() {
        game.subscribe("get_teamstatus", function (GUID, content) {
            if (game.status.team && game.status.job) {
                game.publish("teamstatus", game.status.current);
            }
        });
        game.publish("get_teamstatus");
        game.subscribe("teamstatus", this.updateTeam);
    },

    onDestroyEvent: function() {
        game.unsubscribe("teamstatus", this.updateTeam);
    }
});

game.SelectScreen = me.ScreenObject.extend({
    onResetEvent: function() {
        me.video.clearSurface(me.video.getScreenFrameBuffer(), "black");

        var colors = [
            "red",
            "green",
            "blue",
            "purple",
            "yellow",
            "orange"
        ];

        game.status.GUIDs = {};
        game.status.teams = {};
        colors.forEach(function (color) {
            game.status.teams[color] = {
                "buyer"     : [],
                "farmer"    : [],
                "transport" : []
            };
        })

        // Subscribe to the "teams" event
        game.subscribe("teams", this.addToTeam);

        var w = ~~(c.WIDTH / 3),
            h = ~~(c.HEIGHT / 2),
            team;

        for (var i = 0; i < 6; i++) {
            team = new game.TeamObject((i % 3) * w + 10, Math.floor(i / 3) * h + 10, colors[i]);
            me.game.add(team);
        }
    },

    addToTeam: function (GUID, content) {
        // When all jobs on our team have been fulfilled...
        (function () {
            if (game.status.jobs_filled(content.team)) {
                me.state.change(me.state.PLAY);
                return;
            }

            me.game.getEntityByName(content.team)[0].set();
        }).defer();
    },

    onDestroyEvent: function() {
        game.unsubscribe("teams", this.addToTeam);
    }
});
