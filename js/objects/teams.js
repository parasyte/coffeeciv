game.TeamObject = me.SpriteObject.extend({
    "init" : function init(x, y, color) {
        this.name = color;

        var w = ~~(c.WIDTH / 3) - 20,
            h = ~~(c.HEIGHT / 2) - 20;

        this.font = new me.Font("bold Century Gothic", Math.min(h / 2, w / 4), "black");

        var canvas = document.createElement("canvas");
        canvas.width = w;
        canvas.height = h;
        this.img = canvas.getContext("2d");

        // Superconstructor
        this.parent(x, y, canvas, w, h);

        // Button color
        this.setColor(color);

        // Register click events
        me.input.registerMouseEvent("mousedown", this, this.click.bind(this));
    },

    "click" : function click() {
        if (game.status.team) {
            return;
        }

        var self = this;

        game.status.team = self.name;
        game.status.job = game.status.get_job(game.status.team);
        game.status.position = game.status.get_position();
        console.log("CLICK!", game.status.team, game.status.job, game.status.position);

        game.status.teams[this.name][game.status.job].push(c.GUID);

        // Publish team name (color) and job
        game.publish("teams", {
            "team"  : game.status.team,
            "job"   : game.status.job
        });

        if (game.status.jobs_filled(self.name)) {
            me.state.change(me.state.PLAY);
            return;
        }

        Object.keys(game.status.teams).forEach(function (color) {
            if (color != self.name) {
                me.game.getEntityByName(color)[0].setColor("gray");
            }
        });

        self.set();
    },

    "update" : function update() {
        if (this.updated) {
            this.updated = false;
            return true;
        }
        return false;
    },

    "setColor" : function setColor(color) {
        self.updated = true;
        this.img.fillStyle = color;
        this.img.fillRect(0, 0, this.width, this.height);

        // Draw team name
        this.font.draw(this.img, this.name, 3, 3);
    },

    "set" : function set() {
        var self = this,
            num = 0;

        Object.keys(game.status.teams[self.name]).forEach(function (job) {
            num += game.status.teams[self.name][job].length;
        });

        self.updated = true;
        self.img.fillStyle = self.name;
        self.img.fillRect(self.width * 0.8, 0, self.width * 0.2, self.height);
        self.font.draw(self.img, num, self.width * 0.8, 3);
    }
});
